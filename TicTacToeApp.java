import java.util.Scanner;

public class TicTacToeApp {
    public static void main(String[] args) {
            Scanner reader = new Scanner(System.in);

            System.out.println("Welcome to Tic Tac Toe!");

            // initialize variables for game
            Board g = new Board(); 
            boolean gameOver = false;
            int player = 1;
            Tile playerToken = Tile.X;


            while (gameOver == false) {

                System.out.println("---------------------------------");

                System.out.println(g);

                 // depending on which player's turn it is, change the playertoken to X or O
                 if (player == 1) {
                    playerToken = Tile.X;
                }
                else if (player == 2) {
                    playerToken = Tile.O;
                }
                else {
                    System.out.print("Something went wrong while changing the playerToken");
                }


                // get the location where the player wants to place their token 
                System.out.println("Player " + player + ",where would you like to place your " + playerToken + " token? (1-3)");

                System.out.print("-- Row: "); 
                int placeRow = reader.nextInt();

                System.out.print("-- Column: ");
                int placeCol = reader.nextInt();     

               
                // place token if possible
                boolean tokenPlaced = g.placeToken(placeRow, placeCol, playerToken);

                while (tokenPlaced == false) {
                    System.out.println("Please retry and make sure that the tile chosen exists and is currently blank.");
                    System.out.print("-- Row: "); 
                    placeRow = reader.nextInt();

                    System.out.print("-- Column: ");
                    placeCol = reader.nextInt();    
                    tokenPlaced = g.placeToken(placeRow, placeCol, playerToken);
                }

                // check if there is a winner

                if (g.checkIfWinning(playerToken) == true) {
                    System.out.println("Player " + player + " wins!");
                    gameOver = true;
                }
                else if (g.checkIfFull() == true) {
                    System.out.println("It's a tie!");
                    gameOver = true;
                }
                // move to the next player's turn
                else {
                    if (player == 1) {
                        player++;
                    }
                    else if (player == 2) {
                        player = 1;
                    }
                    else {
                        System.out.println("There was an error changing the player.");
                    }
                }

               


                



            }



            
    }
}
