public class Board {
    private Tile[][] grid;
    private int rows = 3;
    private int cols = 3;

    public Board() {

        grid = new Tile[rows][cols];

        // set all tiles to be blank
       for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                grid[i][j] = Tile.BLANK;
            }
       }
    }


    public String toString() {
        String board = "";
        for(int i = 0; i < cols; i++) {
            for(int j = 0; j < rows; j++) {
                board = board + grid[i][j].getName();
            }
            board = board + "\n"; // go to a new line after each row
        }
        return board;
    }

    public boolean placeToken(int row, int col, Tile playerToken) {

        boolean checkValue = false;
        boolean placed = false;

        // change so that its compatible with starting at 0
        int rowPlace = row - 1;
        int colPlace = col - 1;


        // check if the value is under 3 for row and col
        if((rowPlace < rows) && (rowPlace >= 0) && (colPlace < cols) && (colPlace >= 0)) {
                checkValue = true;
        } 

        // change tile to playertoken if blank
        if (checkValue == true) {
            if (grid[rowPlace][colPlace] == Tile.BLANK) {
                grid[rowPlace][colPlace] = playerToken;
                placed = true;
            }
        }
        return placed;
    }

    // checks if the board is full
    public boolean checkIfFull() {
        boolean isBlank = true;

        // if a tile is blank, the board isn't full
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                if (grid[i][j] == Tile.BLANK) {
                    isBlank = false;
                }
            }
       }
        return isBlank;
    }


    // checks for horizontal win
    private boolean checkIfWinningHorizontal(Tile playerToken) {
        boolean completedRow = false;
        int playerTiles = 0;

        for(int i = 0; i < rows; i++) {
            
            for(int j = 0; j < cols; j++) {
                
                // if column in row is equal to playerToken, playerTiles += 1
                if (grid[i][j] == playerToken) {
                    playerTiles++;
                }

                // checks if theyve complete a row
                if (playerTiles == rows) {
                    completedRow = true;
                }

            }
            // if they haven't, reset for the next row
            playerTiles = 0;
       }
       
       return completedRow;
    }


    // checks for vertical win
    private boolean checkIfWinningVertical(Tile playerToken) {
        boolean completedCol = false;
        int playerTiles = 0;

        for(int i = 0; i < cols; i++) {
            for(int j = 0; j < rows; j++) {
                
                // if row at column is equal to playerToken, playerTiles += 1
                if (grid[j][i] == playerToken) {
                    playerTiles++;
                    System.out.println(playerTiles);
                }

                // checks to see if they've completed a column
                if (playerTiles == cols) {
                    completedCol = true;
                }
            }
            playerTiles = 0;
       }

       return completedCol;
    }


    // checks if either horizontal or vertical win is true
    public boolean checkIfWinning(Tile playerToken) {
        boolean win = false;

        if ((checkIfWinningHorizontal(playerToken) == true) || (checkIfWinningVertical(playerToken) == true)) {
            win = true;
        }

        return win;
    }



}



